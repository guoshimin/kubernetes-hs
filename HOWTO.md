To generate the code:

1. Download https://github.com/guoshimin/gen/archive/haskell-client.zip and extract to ${KUBEGEN}
2. Set `${OUTPUT_DIR}` to where you want the generated code to be. Run

    ```
    PACKAGE_NAME=kubernetes CLIENT_VERSION=unused KUBERNETES_BRANCH=release-1.8 ${KUBEGEN}/gen-haskell-client/openapi/haskell.sh ${OUTPUT_DIR} /dev/null
    ```
3. The codegen doesn't know how to set the package version and license. Edit these manually.